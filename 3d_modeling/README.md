# 3d_modeling theater

## 概要
  この作品は、[tokyo 7th sisters](http://t7s.jp/)に登場する劇場を、可能な限り再現する目的で制作しました<br>
  3D-CADを使用した設計技術により実現しました
  正確な設計図を欠いた状態から、想定している形状を設計し、寸法などを正確に取ることが可能です


### 特徴
- 本作品は[DesignSpark Mechanical](https://www.rs-online.com/designspark/mechanical-software-jp)によりモデリング
- 写真を元に3次元構造を推定し造形


# 開発環境
- OS: Windows 10
- Modeling: DesignSpark Mechanical


# 参考文献

[DesignSpark Mechanical](https://www.rs-online.com/designspark/mechanical-software-jp)<br>
