int x;
String state = "off";

void setup(){
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  x = analogRead(0);          //A0ピンからアナログ入力を行う
  Serial.println(x);

  //感圧センサは触れられると500を下回る
  if(state == "off" && x < 300){
    digitalWrite(13,HIGH);    //13番ピンを5V出力
    state = "on";
  }
  //通常時は1023
  else if(state == "on" && 700 < x ){
    digitalWrite(13, LOW);    //13番ピンを0V出力
    state = "off";
  }
}
